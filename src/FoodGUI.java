import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class FoodGUI {
    private JPanel root;
    private JButton curryButton;
    private JButton naanButton;
    private JButton banhmiButton;
    private JTextPane OrderedItems;
    private JButton phoButton;
    private JButton tacoButton;
    private JButton lassiButton;
    private JButton CheckOut;
    private JLabel TotalPrice;
    private JButton couponButton;
    private JButton sizeButton;
    int total;  //合計金額
    int count_lassi = 0;  //ラッシーの数を計算
    int count_food = 0;  //ラッシー以外の注文を計算
    int count_las_dis = 0;  //割引回数を保持
    String lastOrderedfood; //最後の注文を保持

    void order(String food){    //orderメソッド
        int order_price;
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
//        Yesは0を返す、Noは1を返す
        if(confirmation == 0) {
            JOptionPane.showMessageDialog(
                    null,
                    "Thank you for ordering " + food + "! It will be served as soon as possible.");

//            price(food)で値段を取得
            order_price = price(food);

//            現在時刻を取得
            LocalDateTime nowDate = LocalDateTime.now();
            DateTimeFormatter dtf1 =
                    DateTimeFormatter.ofPattern("HH:mm");
            String formatNowDate = dtf1.format(nowDate);

//            OrderedItemsに商品と現在時刻を表示
            String nowItems = OrderedItems.getText();
            OrderedItems.setText(nowItems+food+"  "+order_price+" yen "+"["+formatNowDate+"]"+"\n");

//            TotalPriceに反映
            total += order_price;
            TotalPrice.setText("Total  "+total+" yen");

//            ラッシー割引判定
            if(count_food>0 && count_lassi>0 && count_las_dis<count_lassi && count_food>=count_lassi) {
                lassiDiscount();
                count_las_dis++;
            }
            lastOrderedfood = food; //直近の注文を保持
            sizeButton.setEnabled(true);  //サイズ変更ボタンを有効にする
        }
    }

    void lassiDiscount() {
        int las_dis = 50;    //ラッシー割引額
        String nowItems = OrderedItems.getText();
        OrderedItems.setText(nowItems + "*Lassi_discount  -" + las_dis + " yen\n");
        total -= las_dis;
        TotalPrice.setText("Total  " + total + " yen");
    }

    void buttonEnable(){  //会計以外のボタンを有効にする
        curryButton.setEnabled(true);
        naanButton.setEnabled(true);
        lassiButton.setEnabled(true);
        tacoButton.setEnabled(true);
        phoButton.setEnabled(true);
        banhmiButton.setEnabled(true);
        couponButton.setEnabled(true);
        sizeButton.setEnabled(true);
    }

    void buttonDisable(){  //会計以外のボタンを無効にする
        curryButton.setEnabled(false);
        naanButton.setEnabled(false);
        lassiButton.setEnabled(false);
        tacoButton.setEnabled(false);
        phoButton.setEnabled(false);
        banhmiButton.setEnabled(false);
        couponButton.setEnabled(false);
        sizeButton.setEnabled(false);
    }

    int price(String food){     //値段設定
        if(food == "Curry") {
            count_food++;
            return 750;
        }
        else if(food == "Naan"){
            count_food++;
            return 100;
        }
        else if(food == "Lassi"){
            count_lassi++;
            return 200;
        }
        else if(food == "Taco rice"){
            count_food++;
            return 780;
        }
        else if(food == "Pho"){
            count_food++;
            return 800;
        }
        else if(food == "Banh mi"){
            count_food++;
            return 500;
        }
        return 0;
    }

    public FoodGUI() {

        curryButton.setIcon(new ImageIcon( this.getClass().getResource("curry.jpg")));
        naanButton.setIcon(new ImageIcon( this.getClass().getResource("naan.jpg")));
        lassiButton.setIcon(new ImageIcon( this.getClass().getResource("lassi.jpg")));
        tacoButton.setIcon(new ImageIcon( this.getClass().getResource("taco_rice.jpg")));
        phoButton.setIcon(new ImageIcon( this.getClass().getResource("pho.jpg")));
        banhmiButton.setIcon(new ImageIcon( this.getClass().getResource("banh_mi.jpg")));


        curryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Curry");
            }
        });
        naanButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Naan");
            }
        });
        lassiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Lassi");
            }
        });
        tacoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Taco rice");
            }
        });
        phoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Pho");
            }
        });
        banhmiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Banh mi");
            }
        });
        CheckOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(total>0) {
                    int confirmation = JOptionPane.showConfirmDialog(null,
                            "Would you like to checkout?",
                            "Checkout Confirmation",
                            JOptionPane.YES_NO_OPTION);
                    if (confirmation == 0) {
//                    感謝のメッセージを表示、OrderedItemsをクリア
                        JOptionPane.showMessageDialog(null, "Thank you. The total price is " + total + " yen.");
                        OrderedItems.setText("");
                        total = 0;
                        TotalPrice.setText("Total  " + total + " yen");
                        count_food=0;
                        count_lassi=0;
                        count_las_dis=0;
                        buttonEnable();    //クーポン以外のボタンを有効にする
                    }
                }
                else{
                    JOptionPane.showMessageDialog(null, "You have not ordered anything yet.");
                }
            }
        });
        couponButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(total>0) {
                    int confirmation = JOptionPane.showConfirmDialog(null,
                            "Do you have a discount coupon?\n" +
                                    "(Please do not press this button except when checking out.)",
                            "Coupon Confirmation",
                            JOptionPane.YES_NO_OPTION);
//                    以下クーポン適用時
                    if (confirmation == 0) {
                        JOptionPane.showMessageDialog(null, "Please bring the coupon to the cashier.\n" +
                                "We will give you 10% discount.");
                        int discount = total / 10;
                        String nowItems = OrderedItems.getText();
                        OrderedItems.setText(nowItems+"**Coupon  -"+discount+" yen");
                        total -= discount;
                        TotalPrice.setText("Total  "+total+" yen");
                        buttonDisable();   //クーポン以外のボタンを無効にする
                    }
                }
                else{
                    JOptionPane.showMessageDialog(null, "You have not ordered anything yet.");
                }
            }
        });
        sizeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(total>0) {
                    int confirmation = JOptionPane.showConfirmDialog(null,
                            "You can change the size to a large serving for an additional 50 yen.\n" +
                                    "Would you like to change "+lastOrderedfood+" to a large serving?",
                            "Size Confirmation",
                            JOptionPane.YES_NO_OPTION);
//                    以下大盛りに変更時
                    if (confirmation == 0) {
                        JOptionPane.showMessageDialog(null, lastOrderedfood+" was changed to a large serving.");
                        String nowItems = OrderedItems.getText();
                        OrderedItems.setText(nowItems+"**Large serving  +50 yen\n");
                        total += 50;
                        TotalPrice.setText("Total  "+total+" yen");
                        sizeButton.setEnabled(false);
                    }
                }
                else{
                    JOptionPane.showMessageDialog(null, "You have not ordered anything yet.");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
